# 三方开源软件windows-rs

## windows-rs简介
windows-rs是微软官方维护的Rust三方库。该库将Windows系统调用的C API进行Rust封装，允许其他Rust库调用Windows系统底层接口。

## 引入背景简述
OpenHarmony的HDC工具需要用到rust异步框架ylong_runtime中的功能，因此需要支持signal/iocp等等Windows的系统调用来向上支持HDC等工具的调用。

任何Rust语言编写的组件需要用到Windows系统调用的场景均可使用。

## 如何使用

在您的BUILD.gn需要的地方添加依赖即可：
```
deps += [ "third_party/rust/crates/libs/sys/windows_sys:lib" ]
```
windows-rs原生使用文档: [README.md](README.md)。
